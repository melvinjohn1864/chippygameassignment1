package com.example.chippy;

import android.content.Context;
import android.graphics.Rect;

public class Square {

    public int xPosition;
    public int yPosition;
    public int width;
    private int speed;

    private Rect hitbox;

    public Square(Context context, int x, int y, int width, int speed) {
        this.xPosition = x;
        this.yPosition = y;
        this.width = width;
        this.speed = speed;

        this.hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + width,
                this.yPosition + width
        );
    }

    public int getSpeed() {
        return this.speed;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Rect getHitbox() {
        return hitbox;
    }

    public void setHitbox(Rect hitbox) {
        this.hitbox = hitbox;
    }

    public void updateHitbox() {
        this.hitbox.left = this.xPosition;
        this.hitbox.top = this.yPosition;
        this.hitbox.right = this.xPosition + width;
        this.hitbox.bottom = this.yPosition + width;
    }
}
