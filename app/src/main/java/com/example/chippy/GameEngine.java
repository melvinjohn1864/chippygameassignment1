package com.example.chippy;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Chronometer;

import java.util.ArrayList;
import java.util.List;

import rb.popview.PopField;

public class GameEngine extends SurfaceView implements Runnable {

    // screen size
    int screenHeight;
    int screenWidth;

    Thread gameThread;
    boolean gameIsRunning;

    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    Context context;

    int rectXPosition;
    int rectYPosition;

    Chips chips;
    Enemy enemy;

    UserControls userControlsUp;
    UserControls userControlsDown;
    UserControls userControlsRight;
    UserControls userControlsLeft;
    ShootControl shootControl;

    Player player;

    boolean isMoving = false;

    private double xn;
    private double yn;

    int numLoops = 0;

    private List<Rect> surroudingRects;
    private List<Rect> coreRects;

    private boolean isSurroundingHit = false;
    private boolean isCoreHit = false;

    private Rect surroundRect;
    private Rect coreRect;

    private boolean isYellowSurroundHit = false;

    //Global dec
    int environmentLoop =0;

    private Chronometer chronometer;

    PopField popField;

    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();
        this.context = context;

        chronometer = new Chronometer(context);

        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();

        surroudingRects = new ArrayList<>();
        coreRects = new ArrayList<>();

        popField = PopField.attach2Window((Activity) context);

        this.screenWidth = w;
        this.screenHeight = h;

        rectXPosition = screenWidth / 2;
        rectYPosition = screenHeight / 2;

        player = new Player(context, 100, screenHeight / 2, "");

        chips = new Chips(context, rectXPosition, rectYPosition);
        enemy = new Enemy(context, rectXPosition, rectYPosition);

        player.setLives(5);


        this.userControlsUp = new UserControls(getContext(), screenWidth - 400, screenHeight - 500);
        this.userControlsDown = new UserControls(getContext(), screenWidth - 400, screenHeight - 300);
        this.userControlsLeft = new UserControls(getContext(), screenWidth - 500, screenHeight - 400);
        this.userControlsRight = new UserControls(getContext(), screenWidth - 300, screenHeight - 400);

        this.shootControl=new ShootControl(getContext(),100,screenHeight-400);

    }


    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }

    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public void updatePositions() {
        // 3. move the bullet
        int x = (int) (xn * 15);
        int y = (int) (yn * 15);

//        int x1 = (int) (xn * 5);
//        int y1 = (int) (yn * 5);

        numLoops = numLoops + 1;



        chips.setRadius(chips.getRadius() + 20);

        //enemy.setLineXPosition(enemy.getLineXPosition() + x);
        //enemy.setLineYPosition(enemy.getLineYPosition() + y);

        if (!isMoving) {

            chips.setxCoord(chips.getxCoord() + 30);
            chips.setyCoord(chips.getyCoord() + 30);
            isMoving = true;

        } else if (isMoving) {
            chips.setxCoord(chips.getxCoord() - 30);
            chips.setyCoord(chips.getyCoord() - 30);
            isMoving = false;
        }

        if (player.getAction() == "up") {
            // if mouseup, then move player up
            player.setyPosition(player.getyPosition() - 100);
            player.updateHitbox();
            player.setAction("");
        } else if (player.getAction() == "down") {
            // if mousedown, then move player down
            player.setyPosition(player.getyPosition() + 100);
            player.updateHitbox();
            player.setAction("");

        } else if (player.getAction() == "left") {
            // if mouseleft, then move player left
            player.setxPosition(player.getxPosition() - 100);
            player.updateHitbox();
            player.setAction("");

        } else if (player.getAction() == "right") {
            // if mouseright, then move player right
            player.setxPosition(player.getxPosition() + 100);
            player.updateHitbox();
            player.setAction("");
        }



        for (int i = 0; i < this.player.getBullets().size();i++) {
            Square bullet = this.player.getBullets().get(i);

            if (surroudingRects.size() != 0){
                checkCollisionSurroundingRects(bullet);
            }else if (coreRects.size() != 0){
                checkCollisionCoreRects(bullet);
            }else {
                checkInitialCollisionSurroundingRects(bullet);
                checkInitialCollisionCoreRects(bullet);

            }

        }

        if (enemy.getEnvironmentRects().size() != 0){
            for (int i = 0; i < this.enemy.getEnvironmentRects().size();i++) {
                Rect environmentRect = this.enemy.getEnvironmentRects().get(i);

                if (player.getHitbox().intersect(environmentRect)){
                    player.setLives(player.getLives() - 1);
                }

            }
        }

        if (player.getPowerUpRects().size() != 0){
            for (int i = 0; i < this.player.getPowerUpRects().size();i++) {
                Rect powerUpRect = this.player.getPowerUpRects().get(i);

                if (player.getHitbox().intersect(powerUpRect)){
                    player.setLives(player.getLives() + 1);
                }

            }
        }

        if (chips.getEnemyMapCopy() != null && chips.getEnemyMapCopy().get(0) != null && chips.getEnemyMapCopy().get(0).size() != 0){
            for (int i = 0; i < chips.getEnemyMapCopy().get(0).size();i++) {
                Rect fireRect = this.chips.getEnemyMapCopy().get(0).get(i).getHitbox();

                if (player.getHitbox().intersect(fireRect)){
                    player.setLives(player.getLives() - 1);
                }

            }
        }

        //Move environment obstacles
        this.environmentLoop = this.environmentLoop + 1;

        enemy.setEnvironmentX(enemy.getEnvironmentX() + 30);
        if(enemy.getEnvironmentX() > screenWidth){
            enemy.setEnvironmentX(200);
        }

        player.setPowerupX(player.getPowerupX() + 30);
        if(player.getPowerupX() > screenWidth){
            player.setPowerupX(200);
        }

        if (player.getLives() <= 0){
            pauseGame();
        }


    }

    private void checkCollisionSurroundingRects(Square bullet) {
        for (int i = 0; i < this.surroudingRects.size();i++) {
            if (bullet.getHitbox().intersect(surroudingRects.get(i)) == true){
                isYellowSurroundHit = true;
                chips.getSurroundingChips().remove(surroudingRects.get(i));
                surroudingRects.remove(surroudingRects.get(i));
                break;
            }
        }
    }

    private void checkCollisionCoreRects(Square bullet) {
        for (int i = 0; i < this.coreRects.size();i++) {
            if (bullet.getHitbox().intersect(coreRects.get(i)) == true){
                chips.getCoreChips().remove(coreRects.get(i));
                coreRects.remove(coreRects.get(i));
                break;
            }
        }
    }

    private void checkInitialCollisionSurroundingRects(Square bullet) {
        for (int i = 0; i < this.chips.getSurroundingChips().size();i++) {
            if (bullet.getHitbox().intersect(chips.getSurroundingChips().get(i)) == true){
                Log.d("Clicked","true");
                surroundRect = new Rect(chips.getSurroundingChips().get(i).left,
                        chips.getSurroundingChips().get(i).top,
                        chips.getSurroundingChips().get(i).right,
                        chips.getSurroundingChips().get(i).bottom);
                isSurroundingHit = true;
                surroudingRects.add(chips.getSurroundingChips().get(i));
            }
        }
    }

    private void checkInitialCollisionCoreRects(Square bullet) {
        for (int i = 0; i < this.chips.getCoreChips().size();i++) {
            if (bullet.getHitbox().intersect(chips.getCoreChips().get(i)) == true){
                coreRect = new Rect(chips.getCoreChips().get(i).left,
                        chips.getCoreChips().get(i).top,
                        chips.getCoreChips().get(i).right,
                        chips.getCoreChips().get(i).bottom);
                isCoreHit = true;
                coreRects.add(chips.getCoreChips().get(i));
            }
        }
    }

    private void calculateThePatternDistance(int getxPosition, int getyPosition) {
        double a = (getxPosition);
        double b = (getyPosition);
        double distance = Math.sqrt((a * a) + (b * b));

        // 2. calculate the "rate" to move
        xn = (a / distance);
        yn = (b / distance);

    }


    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //counter = counter + 1;

            long chronometerTime = SystemClock.elapsedRealtime() - chronometer.getBase();
            String time = Long.toString(chronometerTime/1000);

            paintbrush.setColor(Color.WHITE);

            canvas.drawBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.background),0,0,null);

//            for (int i = 0; i < this.player.getBullets().size(); i++) {
//                Square bullet = this.player.getBullets().get(i);
//                canvas.drawRect(bullet, paintbrush);
//            }

            setImageForUserControls();

            chips.createEnemyBulletPattern(enemy.getxPosition(), enemy.getyPosition(), 0);

            for (int i = 0; i < chips.getEnemyMap().get(0).size(); i++) {
                canvas.drawBitmap(chips.getEnemyMap().get(0).get(i).getImage(), chips.getEnemyMap().get(0).get(i).getxPosition(), chips.getEnemyList().get(i).getyPosition(), paintbrush);
            }



            canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);

            canvas.drawBitmap(userControlsUp.getImage(), userControlsUp.getxPosition(), userControlsUp.getyPosition(), paintbrush);
            canvas.drawBitmap(userControlsDown.getImage(), userControlsDown.getxPosition(), userControlsDown.getyPosition(), paintbrush);
            canvas.drawBitmap(userControlsLeft.getImage(), userControlsLeft.getxPosition(), userControlsLeft.getyPosition(), paintbrush);
            canvas.drawBitmap(userControlsRight.getImage(), userControlsRight.getxPosition(), userControlsRight.getyPosition(), paintbrush);
            canvas.drawBitmap(shootControl.getImage(), shootControl.getxPosition(), shootControl.getyPosition(), paintbrush);


            for (int i = 0; i < chips.getSurroundingChips().size(); i++) {
                canvas.drawRect(chips.getSurroundingChips().get(i), paintbrush);
            }

            paintbrush.setColor(Color.RED);



            for (int i = 0; i < chips.getCoreChips().size(); i++) {
                canvas.drawRect(chips.getCoreChips().get(i), paintbrush);
            }

            chips.setFirstTime(false);

            paintbrush.setColor(Color.YELLOW);

            if (isSurroundingHit){
                canvas.drawRect(surroundRect,paintbrush);
                isSurroundingHit = false;
            }

            paintbrush.setColor(Color.CYAN);

            if (isCoreHit){
                canvas.drawRect(coreRect,paintbrush);
                isCoreHit = false;
            }

            if (isYellowSurroundHit){
                //popField.popView(yellowSurroundRect);
            }

            paintbrush.setColor(Color.YELLOW);
            paintbrush.setTextSize(100);


            canvas.drawText("Time: " + time, 100, 200,paintbrush);

            canvas.drawText("Lives: " + player.getLives() + "", 750, 200,paintbrush);

            //canvas.drawLine(rectXPosition,rectYPosition,enemy.getLineXPosition(),enemy.getLineYPosition(),paintbrush);

            int colour=getResources().getColor(R.color.colorBullet);

            paintbrush.setColor(colour);

            paintbrush.setStrokeWidth(8);

            // draw bullet
            // paintbrush.setColor(Color.BLACK);

            for (int i = 0; i < player.getBullets().size();i++) {
                Square b = player.getBullets().get(i);
                canvas.drawRect(
                        b.getxPosition(),
                        b.getyPosition(),
                        b.getxPosition() + b.getWidth(),
                        b.getyPosition() + b.getWidth(),
                        paintbrush
                );
            }

            for (int i = 0; i < player.getBullets().size();i++) {
                Square b = player.getBullets().get(i);

                moveBulletToTarget(b);
            }

            int interval=Integer.parseInt(time);

            paintbrush.setColor(Color.RED);


            if((interval>10&&interval<20)||(interval>30&&interval<40)){
                for (int i = 0; i < enemy.createEnvironment().size(); i++) {
                    canvas.drawRect(enemy.createEnvironment().get(i), paintbrush);
                    Log.d("leftSprites", "redrawSprites: ");

                }}


            //canvas.drawBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.background),0,0,null);
            int colourPower=getResources().getColor(R.color.colorPower);
            paintbrush.setColor(colourPower);
            if((interval>20&&interval<30)||(interval>50&&interval<60)){
                for (int i = 0; i < player.createPowerups().size(); i++) {
                    canvas.drawRect(player.createPowerups().get(i), paintbrush);
                    Log.d("leftPower", "redrawPower: ");

                }}


            this.holder.unlockCanvasAndPost(canvas);

        }
    }

    private void setImageForUserControls() {
        Matrix mat = new Matrix();
        mat.postRotate(90);
        Bitmap downArrow = Bitmap.createBitmap(userControlsRight.getImage(), 0, 0,
                userControlsRight.getImage().getWidth(), userControlsRight.getImage().getHeight(),
                mat, true);
        userControlsDown.setImage(downArrow);
        Bitmap leftArrow = Bitmap.createBitmap(downArrow, 0, 0,
                downArrow.getWidth(), downArrow.getHeight(),
                mat, true);
        userControlsLeft.setImage(leftArrow);
        Bitmap upArrow = Bitmap.createBitmap(leftArrow, 0, 0,
                leftArrow.getWidth(), leftArrow.getHeight(),
                mat, true);
        userControlsUp.setImage(upArrow);
    }


    public void setFPS() {
        try {
            gameThread.sleep(20);
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {

        if (e.getY() < userControlsUp.getyPosition() + userControlsUp.getImage().getHeight() && e.getY() > userControlsUp.getyPosition()) {
            player.setAction("up");
        } else if (e.getY() < userControlsDown.getyPosition() + userControlsDown.getImage().getHeight() && e.getY() > userControlsDown.getyPosition()) {
            player.setAction("down");
        } else if (e.getX() < userControlsLeft.getxPosition() + userControlsLeft.getImage().getWidth() && e.getX() > userControlsLeft.getxPosition()) {
            player.setAction("left");
        } else if (e.getX() < userControlsRight.getxPosition() + userControlsRight.getImage().getWidth() && e.getX() > userControlsRight.getxPosition()) {
            player.setAction("right");
        }else if (e.getX() < shootControl.getxPosition() + shootControl.getImage().getWidth() && e.getX() > shootControl.getxPosition()) {
            player.bulletGenerate();



        }

        return true;
    }


    public void moveBulletToTarget(Square bulletGet) {
        // @TODO:  Move the square
        // 1. calculate distance between bullet and square
        double a = ((rectXPosition + 300) - bulletGet.xPosition);
        double b = ((rectYPosition + 100) - bulletGet.yPosition);
        double distance = Math.sqrt((a*a) + (b*b));

        // 2. calculate the "rate" to move
        double xn = (a / distance);
        double yn = (b / distance);

        // 3. move the bullet
        bulletGet.xPosition = bulletGet.xPosition + (int)(xn * bulletGet.getSpeed());
        bulletGet.yPosition = bulletGet.yPosition + (int)(yn * bulletGet.getSpeed());

        bulletGet.updateHitbox();
    }



}
