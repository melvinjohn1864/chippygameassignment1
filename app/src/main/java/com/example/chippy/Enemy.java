package com.example.chippy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


public class Enemy {

    private int xPosition;
    private int yPosition;
    private Bitmap image;
    Context context;
    private int lineXPosition = xPosition;
    private int lineYPosition = yPosition;

    private int environmentX = 200;
    private int environmentY = 1200;

    private List<Rect> environmentRects;

    private Rect hitbox;

    public Enemy(Context context,int xPosition, int yPosition) {
        this.context = context;
        this.xPosition = xPosition;
        this.yPosition = yPosition;

        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.fire1);

        environmentRects = new ArrayList<>();

        this.hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.image.getWidth(),
                this.yPosition + this.image.getHeight()
        );
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public int getLineXPosition() {
        return lineXPosition;
    }

    public void setLineXPosition(int lineXPosition) {
        this.lineXPosition = lineXPosition;
    }

    public int getLineYPosition() {
        return lineYPosition;
    }

    public void setLineYPosition(int lineYPosition) {
        this.lineYPosition = lineYPosition;
    }

    public int getEnvironmentX() {
        return environmentX;
    }

    public void setEnvironmentX(int environmentX) {
        this.environmentX = environmentX;
    }

    public int getEnvironmentY() {
        return environmentY;
    }

    public void setEnvironmentY(int environmentY) {
        this.environmentY = environmentY;
    }

    public List<Rect> getEnvironmentRects() {
        return environmentRects;
    }

    public void setEnvironmentRects(List<Rect> environmentRects) {
        this.environmentRects = environmentRects;
    }

    public Rect getHitbox() {
        return hitbox;
    }

    public void setHitbox(Rect hitbox) {
        this.hitbox = hitbox;
    }

    //Environment Creation
    public List<Rect> createEnvironment(){
        List<Rect> rects = new ArrayList<>();
        // Log.d("arrayliost", "array: ");
//                this.environmentX = 200;
//                this.environmentY = 1200;
        for (int i = 0;i < 3;i++){
            Rect rect = new Rect(environmentX  , environmentY - 250 - 400*i, environmentX + 50  ,environmentY - 400 * i - 310);
            Log.d("environment", "createEnvironment: at side ");

            rects.add(rect);
            environmentRects.addAll(rects);
        }
        return rects;
    }

    public void updateHitbox() {
        this.hitbox.left = this.xPosition;
        this.hitbox.top = this.yPosition;
        this.hitbox.right = this.xPosition + this.image.getWidth();
        this.hitbox.bottom = this.yPosition + this.image.getHeight();
    }

}
